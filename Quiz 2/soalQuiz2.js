/**
 * Berikut soal quiz kali ini, terdiri dari 3 Soal
 * Kerjakan dengan sebaik mungkin, dengan menggunakan metode yang telah dipelajari,
 * Tidak diperkenankan untuk menjawab hanya dengan console.log('teks jawaban');
 * maupun dengan terlebih dahulu memasukkannya ke dalam variabel, misal var a = 'teks jawaban'; console.log(a);
 * 
 * Terdapat tambahan poin pada setiap soal yang dikerjakan menggunakan sintaks ES6 (+5 poin)
 * Jika total nilai Anda melebihi 100 (nilai pilihan ganda + coding), tetap akan memiliki nilai akhir yaitu 100
 * 
 * Selamat mengerjakan
*/

/*========================================== 
  1. SOAL CLASS SCORE (10 poin + 5 Poin ES6)
  ==========================================
  Buatlah sebuah class dengan nama Score. class Score tersebut memiliki properti "subject", "points", dan "email". 
  "points" dapat di input berupa number (1 nilai) atau array of number (banyak nilai).
  tambahkan method average untuk menghitung rata-rata dari parameter points ketika yang di input berupa array (lebih dari 1 nilai)
*/

class Score {
  constructor(subject, points, email){
    var array = []
    array = array.concat(points)
    this.subject = subject
    this.points = array
    this.email = email
  }
  get average(){
      var temp = 0
      for (let i = 0; i<this.points.length; i++){
        temp += +this.points[i]
      }
      return temp/this.points.length
  }
}
/*
Test
const apalah = new Score("Amel", ["25","15"], "amel@gmail")
console.log(apalah)
console.log(apalah.average)
*/
/*=========================================== 
  2. SOAL Create Score (10 Poin + 5 Poin ES6)
  ===========================================
  Membuat function viewScores yang menerima parameter data berupa array multidimensi dan subject berupa string
  Function viewScores mengolah data email dan nilai skor pada parameter array 
  lalu mengembalikan data array yang berisi object yang dibuat dari class Score.
  Contoh: 

  Input
   
  data : 
  [
    ["email", "quiz-1", "quiz-2", "quiz-3"],
    ["abduh@mail.com", 78, 89, 90],
    ["khairun@mail.com", 95, 85, 88]
  ]
  subject: "quiz-1"

  Output 
  [
    {email: "abduh@mail.com", subject: "quiz-1", points: 78},
    {email: "khairun@mail.com", subject: "quiz-1", points: 95},
  ]
*/

const data = [
  ["email", "quiz - 1", "quiz - 2", "quiz - 3"],
  ["abduh@mail.com", 78, 89, 90],
  ["khairun@mail.com", 95, 85, 88],
  ["bondra@mail.com", 70, 75, 78],
  ["regi@mail.com", 91, 89, 93]
]
/*
Test
const apaini = new Score("quiz - 2", data[1][data[0].indexOf("quiz - 2")], data[1][0])
console.log(apaini)
console.log(data[0].indexOf("quiz - 1"))
*/

function viewScores(data, subject) {
var array = []
for (var i=1; i<data.length; i++){
  object = new Score(subject,data[i][data[0].indexOf(subject)],data[i][0])
  temp = {
    email : object.email,
    subject : object.subject,
    points : object.points[0]
  }
  array.push(temp)
}
  return console.log(array)
}

// TEST CASE
viewScores(data, "quiz - 1")
viewScores(data, "quiz - 2")
viewScores(data, "quiz - 3")

/*==========================================
  3. SOAL Recap Score (15 Poin + 5 Poin ES6)
  ==========================================
    Buatlah fungsi recapScore yang menampilkan perolehan nilai semua student. 
    Data yang ditampilkan adalah email user, nilai rata-rata, dan predikat kelulusan. 
    predikat kelulusan ditentukan dari aturan berikut:
    nilai > 70 "participant"
    nilai > 80 "graduate"
    nilai > 90 "honour"

    output:
    1. Email: abduh@mail.com
    Rata-rata: 85.7
    Predikat: graduate

    2. Email: khairun@mail.com
    Rata-rata: 89.3
    Predikat: graduate

    3. Email: bondra@mail.com
    Rata-rata: 74.3
    Predikat: participant

    4. Email: regi@mail.com
    Rata-rata: 91
    Predikat: honour

*/
class scoreHit extends Score {
  constructor(subject, points, email){
    super(subject,points,email)
  }
  predikat(){
    if (this.average<=80){
      return "participant"
    } else if (this.average>80 && this.average<=90){
      return "graduate"
    } else if (this.average>90){
      return "honour"
    }
  }
}
//const apalagi = new scoreHit("whatever",[12, 13, 14],"apalah@gmail")
//console.log(apalagi)
//console.log(apalagi.predikat())

function recapScores(data) {
  for (var i = 1; i<data.length; i++){
    object = new scoreHit("whatever",data[i].slice(1,4),data[i][0])
    var temp = ""
    temp += `${i}. Email: ${object.email}\nRata-rata: ${Math.round(object.average*10)/10}\nPredikat: ${object.predikat()}\n`
    console.log(temp)
  }
}

recapScores(data);
