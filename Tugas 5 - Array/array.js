//variabel umum
var batas = "------------------------"
function judul(NomorSoal){
    console.log(batas)
    console.log("Output Soal No. "+NomorSoal)
}
//variabel untuk soal nomor 1
//variabel untuk soal nomor 2
//variabel untuk soal nomor 3

//koding untuk soal nomor 1
judul(1)
function range(awal, akhir){
    var array1 = []
    if (awal<=akhir){
        for (var i=awal;i<=akhir;i++){
            array1.push(i)
        }
        return array1
    } else if (awal>akhir){
        for (var i=awal;i>=akhir;i--){
            array1.push(i)
        }
        return array1
    } else {return -1}
}
console.log(range(1,10))
console.log(range(1))
console.log(range(11,18))
console.log(range(54,50))
console.log(range())

//koding untuk soal nomor 2
judul(2)
function rangeWithStep(awal, akhir, step = 1){
    var array2 = []
    if (awal<=akhir){
        for (var i=awal;i<=akhir;i+=step){
            array2.push(i)
        }
        return array2
    } else if (awal>akhir){
        for (var i=awal;i>=akhir;i-=step){
            array2.push(i)
        }
        return array2
    } else {return -1}
}
console.log(rangeWithStep(1,10,2))
console.log(rangeWithStep(11,23,3))
console.log(rangeWithStep(5,2,1))
console.log(rangeWithStep(29,2,4))

//koding untuk soal nomor 3
judul(3)
function sum(awal, akhir, step){
    var array3 = []
    var jumlah = awal
    array3 = rangeWithStep(awal,akhir,step)
    for (var j=1;j<(array3.length);j++){
        jumlah += array3[j]
    }
    if ((awal==null)&&(akhir==null)&&(step==null)){
        jumlah = 0
    }
    return jumlah
}
console.log(sum(1,10))
console.log(sum(5,50,2))
console.log(sum(15,10))
console.log(sum(20,10,2))
console.log(sum(1))
console.log(sum())

//koding untuk soal nomor 4
judul(4)
var input = [
    ["0001","Roman Alamsyah","Bandar Lampung","21/05/1989","Membaca"],
    ["0002","Dika Sembiring","Medan","10/10/1992","Bermain Gitar"],
    ["0003", "Winona", "Ambon", "25/12/1965", "Memasak"],
    ["0004", "Bintang Senjaya", "Martapura", "6/4/1970", "Berkebun"]
]
function dataHandling(array){
    for (i=0;i<array.length;i++){
        console.log("Nomor ID: "+array[i][0])
        console.log("Nama Lengkap: "+array[i][1])
        console.log("TTL: "+array[i][2]+" "+array[i][3])
        console.log("Hobi: "+array[i][4]+"\n")
    }
}
dataHandling(input)

//koding untuk soal nomor 5
judul(5)
function balikKata(kata){
    var temp = ""
    for (i=(kata.length-1);i>=0;i--){
        temp += kata[i]
        //temp.push(i)
    }
    return temp
}
console.log(balikKata("Kasur Rusak"))
console.log(balikKata("SanberCode"))
console.log(balikKata("Haji Ijah"))
console.log(balikKata("racecar"))
console.log(balikKata("I am Sanbers"))

//koding untuk soal nomor 6
judul(6)
var input2 = ["0001","Roman Alamsyah","Bandar Lampung","21/05/1989","Membaca"]
function dataHandling2(array){
    array.splice(4,1,"Pria")
    array.push("SMA Internasional Metro")
    array[1] += " Elsyarawy"
    array[2] = "Provinsi "+array[2]
    console.log(array)
    
    tanggal = array[3].split("/")
    tanggalCopy = tanggal.slice()

    bulan = parseInt(tanggal[1])
    switch(bulan) {
        case 1:   { bulan = "Januari"; break; }
        case 2:   { bulan = "Februari"; break; }
        case 3:   { bulan = "Maret"; break; }
        case 4:   { bulan = "April"; break; }
        case 5:   { bulan = "Mei"; break; }
        case 6:   { bulan = "Juni"; break; }
        case 7:   { bulan = "Juli"; break; }
        case 8:   { bulan = "Agustus"; break; }
        case 9:   { bulan = "September"; break; }
        case 10:   { bulan = "Oktober"; break; }
        case 11:   { bulan = "November"; break; }
        case 12:   { bulan = "Desember"; break; }
        default:  { bulan = "**salah bulan!**"; }
    }
    console.log(bulan)
    
    tanggal.sort(function(a,b){return b-a})
    console.log(tanggal)
    console.log(tanggalCopy.join("-"))
    
    array[1] = array[1].slice(0,14)
    console.log(array[1])
}
dataHandling2(input2)