//variabel umum
var batas = "----------------------"
//variabel untuk Soal No.1
//variabel untuk Soal No.2
var num1 = 12
var num2 = 4
//variabel untuk Soal No.3
var name = "Agus"
var age = 30
var address = "Jln. Malioboro, Yogyakarta"
var hobby = "Gaming"

//koding untuk Soal No. 1
console.log(batas)
console.log("Output Soal No.1")
function teriak(){
    return "Halo Sanbers!"
}
console.log(teriak());

//koding untuk Soal No. 2
console.log(batas)
console.log("Output Soal No.2")
function kalikan(angka1, angka2){
    return angka1*angka2 
}
var hasilKali = kalikan(num1,num2)
console.log(hasilKali);

//koding untuk Soal No. 3
console.log(batas)
console.log("Output Soal No.3")
function introduce(nama, umur, alamat, hobi){
    return "Nama saya "+nama+", umur saya "+umur+" tahun, alamat saya di "+alamat+", dan saya punya hobby yaitu "+hobi+"!"
}
var perkenalan = introduce(name,age,address,hobby);
console.log(perkenalan);