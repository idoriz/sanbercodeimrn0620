//variabel umum
var batas = "------------------------"
function judul(NomorSoal){
    console.log(batas)
    console.log("Output Soal No. "+NomorSoal)
}
var now = new Date()
var thisYear = now.getFullYear()

judul(1)
function arrayToObject(arr) {
    var temp = {}
    if (arr.length == 0){
        console.log('""')
    } else {
        for (var i = 0; i<arr.length; i++){
            console.log((i+1)+". "+arr[i][0]+" "+arr[i][1]+":")
            temp.firstName = arr[i][0]
            temp.lastName = arr[i][1]
            temp.gender = arr[i][2]
            if (thisYear>=arr[i][3]){
                temp.age = thisYear-arr[i][3]
            } else {temp.age = "Invalid Birth Year"}
            console.log(temp)
        }
    }
     
}
 
// Driver Code
var people = [ ["Bruce", "Banner", "male", 1975], ["Natasha", "Romanoff", "female"] ]
arrayToObject(people) 
/*
    1. Bruce Banner: { 
        firstName: "Bruce",
        lastName: "Banner",
        gender: "male",
        age: 45
    }
    2. Natasha Romanoff: { 
        firstName: "Natasha",
        lastName: "Romanoff",
        gender: "female".
        age: "Invalid Birth Year"
    }
*/
 
var people2 = [ ["Tony", "Stark", "male", 1980], ["Pepper", "Pots", "female", 2023] ]
arrayToObject(people2) 
/*
    1. Tony Stark: { 
        firstName: "Tony",
        lastName: "Stark",
        gender: "male",
        age: 40
    }
    2. Pepper Pots: { 
        firstName: "Pepper",
        lastName: "Pots",
        gender: "female".
        age: "Invalid Birth Year"
    }
*/
 
// Error case 
arrayToObject([]) // ""

judul(2)
function toPrice(barang){
    switch (barang){
        case "Sepatu Stacattu" : {barang = 1500000; break;}
        case "Baju Zoro" : {barang = 500000; break;}
        case "Baju H&N" : {barang = 250000; break;}
        case "Sweater Uniklooh" : {barang = 175000; break;}
        case "Casing Handphone" : {barang = 50000; break;}
        default : {barang = 0;}
    }
    return barang
}

function belanjaan(memberId){
    if (memberId == "1820RzKrnWn08"){
        return ['Sepatu Stacattu','Baju Zoro','Baju H&N','Sweater Uniklooh','Casing Handphone']
    } else if (memberId == '82Ku8Ma742') {
        return ['Casing Handphone']
    }
}

function kembalian (money, belanjaan){
    var temp = money
    for (var i = 0; i<belanjaan.length; i++){
        temp -= toPrice(belanjaan[i])
    }
    return temp
}

function shoppingTime(memberId, money) {
    var temp = {}
    if (memberId === ""||memberId == null){
        return "Mohon maaf, toko X hanya berlaku untuk member saja"
    } else if (money < 50000){
        return "Mohon maaf, uang tidak cukup"
    } else {
        temp = {
            memberId : memberId,
            money : money,
            listPurchased : belanjaan(memberId),
            changeMoney : kembalian(money, belanjaan(memberId))
        }
        return temp
    }
  }
   
  // TEST CASES
  console.log(shoppingTime('1820RzKrnWn08', 2475000));
    //{ memberId: '1820RzKrnWn08',
    // money: 2475000,
    // listPurchased:
    //  [ 'Sepatu Stacattu',
    //    'Baju Zoro',
    //    'Baju H&N',
    //    'Sweater Uniklooh',
    //    'Casing Handphone' ],
    // changeMoney: 0 }
  console.log(shoppingTime('82Ku8Ma742', 170000));
  //{ memberId: '82Ku8Ma742',
  // money: 170000,
  // listPurchased:
  //  [ 'Casing Handphone' ],
  // changeMoney: 120000 }
  console.log(shoppingTime('', 2475000)); //Mohon maaf, toko X hanya berlaku untuk member saja
  console.log(shoppingTime('234JdhweRxa53', 15000)); //Mohon maaf, uang tidak cukup
  console.log(shoppingTime()); ////Mohon maaf, toko X hanya berlaku untuk member saja

  judul(3)
  function naikAngkot(arrPenumpang) {
    rute = ['A', 'B', 'C', 'D', 'E', 'F'];
    var array = []
    for (var i = 0; i<arrPenumpang.length; i++){
        temp = {
            penumpang : arrPenumpang[i][0],
            naikDari : arrPenumpang[i][1],
            tujuan : arrPenumpang[i][2],
            bayar : 2000*(rute.indexOf(arrPenumpang[i][2])-rute.indexOf(arrPenumpang[i][1]))
        }
        array.push(temp)
    }
    return array
  }
   
  //TEST CASE
  console.log(naikAngkot([['Dimitri', 'B', 'F'], ['Icha', 'A', 'B']]));
  // [ { penumpang: 'Dimitri', naikDari: 'B', tujuan: 'F', bayar: 8000 },
  //   { penumpang: 'Icha', naikDari: 'A', tujuan: 'B', bayar: 2000 } ]
   
  console.log(naikAngkot([])); //[]
  