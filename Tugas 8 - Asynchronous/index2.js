var readBooksPromise = require('./promise.js')
 
var books = [
    {name: 'LOTR', timeSpent: 3000}, 
    {name: 'Fidas', timeSpent: 8000}, 
    {name: 'Kalkulus', timeSpent: 4000}
]

readBooksPromise(10000,books[0])
    .then(function(fulfilled){
        readBooksPromise(fulfilled,books[1])
            .then(function(fulfilled){
                readBooksPromise(fulfilled,books[2])
                    .then(function(fulfilled){
                        console.log("Selesai")
                    })
                    .catch(function(){
                        console.log("Waktu Habis")
                    })
            })
            .catch(function(){
                console.log("Waktu Habis")
            })
    })
    .catch(function(){
        console.log("Waktu Habis")
    })
// Lanjutkan code untuk menjalankan function readBooksPromise 