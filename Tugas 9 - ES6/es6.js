//judul
//variabel umum
var batas = "------------------------"
function judul(NomorSoal){
    console.log(batas)
    console.log("Output Soal No. "+NomorSoal)
}

/*
// ES5 way
const golden = function goldenFunction(){
    console.log("this is golden!!")
  }
*/   

judul(1);
/*
// ES6 way type 1
const golden = () => {
    return console.log("this is golden!!");
}
*/

// ES6 way type 2
const golden = () => console.log("this is golden!!")

golden()

judul(2);
/*
//ES5 way
const newFunction = function literal(firstName, lastName){
    return {
      firstName: firstName,
      lastName: lastName,
      fullName: function(){
        console.log(firstName + " " + lastName)
        return 
      }
    }
  }
*/
//ES6 way
const newFunction = (firstName, lastName) => {
    return {
      firstName,
      lastName,
      fullName: () => console.log(firstName + " " + lastName)
    }
  }
   
  //Driver Code 
  newFunction("William", "Imoh").fullName() 

judul(3)
const newObject = {
  firstName: "Harry",
  lastName: "Potter Holt",
  destination: "Hogwarts React Conf",
  occupation: "Deve-wizard Avocado",
  spell: "Vimulus Renderus!!!"
}

//ES5
/*
const firstName = newObject.firstName;
const lastName = newObject.lastName;
const destination = newObject.destination;
const occupation = newObject.occupation;
*/

//ES6
const {firstName, lastName, destination, occupation} = newObject
console.log(firstName, lastName, destination, occupation)

judul(4)
const west = ["Will", "Chris", "Sam", "Holly"]
const east = ["Gill", "Brian", "Noel", "Maggie"]
//ES5
//const combined = west.concat(east)

//ES6
let combined = [...west, ...east]
//Driver Code
console.log(combined)

judul(5)
const planet = "earth"
const view = "glass"
//ES5
/*
var before = 'Lorem ' + view + ' dolor sit amet, ' +  
    'consectetur adipiscing elit, ' + planet + ' do eiusmod tempor ' +
    'incididunt ut labore et dolore magna aliqua. Ut enim' +
    ' ad minim veniam'
*/
//ES6
const before = `Lorem ${view} dolor sit amet, consectetur adipiscing elit, ${planet} do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam`

// Driver Code
console.log(before)