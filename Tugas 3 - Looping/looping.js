//variabel umum
var batas = "-----------------"
//variabel untuk Soal No. 1
var flag1 = 2
var flag2 = 20
//variabel untuk Soal No.2
var inisiasi1 = 1
var kondisi1 = 20
//variabel untuk Soal No.3
var temp1 = ""
//variabel untuk Soal No.4
var temp2 = ""
//variabel untuk soal No.5
var temp3 = ""

//koding untuk Soal No.1
console.log(batas)
console.log("Output Soal No.1")
console.log("LOOPING PERTAMA") //judul
while (flag1 <= 20) {
    console.log(flag1+" - I love coding");
    flag1+=2;
}
console.log("LOOPING KEDUA") //judul
while (flag2>0){
    console.log(flag2+" - I will become a mobile developer");
    flag2-=2;
}

//koding untuk Soal No.2
console.log(batas)
console.log("Output Soal No.2")
for (var flag3 = inisiasi1; flag3 <= kondisi1; flag3++){
    if (flag3%2 == 0){
        console.log(flag3+" - Berkualitas");
    } else {
        if (flag3%3 == 0) {
            console.log(flag3+" - I Love Coding");
        } else {
            console.log(flag3+" - Santai");
        }
    }
}

//koding untuk Soal No.3
console.log(batas)
console.log("Output Soal No.3")
for (var panjang = 1; panjang <= 8; panjang++){
    temp1 += "#"
}
for (var lebar = 1; lebar <= 4; lebar++){
    console.log(temp1);
}

//koding untuk Soal No.4
console.log(batas)
console.log("Output Soal No.4")
for (var baris2 = 1; baris2 <= 7; baris2++){
        temp2 += "#"
        console.log(temp2)
}

//koding untuk Soal No.5
console.log(batas)
console.log("Output Soal No.5")
for (var baris3 = 0; baris3 < 8; baris3++){
    for (var kolom3 = 0; kolom3 <= 8; kolom3++){
        if (kolom3 == 8){
            console.log(temp3)
            temp3 = ""
        } else {
            if ((baris3+kolom3)%2 == 0){
                temp3 += " "
            } else {
                temp3 += "#"
            }
        }
    }
    
}