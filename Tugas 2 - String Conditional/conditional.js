//variabel umum
var batas = "--------------------"
//variabel untuk tugas if-else
var nama = "Tukimin" //silakan di-assign, bebas
var peran = "Werewolf" //silakan di-assign dari pilihan antara "Penyihir", "Werewolf", atau "Guard"
//variabel untuk tugas switch case
var hari = 10;
var bulan = 12;
var tahun = 1945;

//Koding untuk tugas if-else
console.log(batas);
console.log("Tugas if-else")
if (nama == ""){
    console.log("Nama harus diisi!")
} else {
    if (peran == ""){
        console.log("Halo "+nama+", Pilih peranmu untuk memulai game!")
    } else if (peran == "Penyihir"){
        console.log("Selamat datang di Dunia Werewolf, "+nama)
        console.log("Halo "+peran+" "+nama+", kamu dapat melihat siapa saja yang menjadi werewolf!")
    } else if (peran == "Guard"){
        console.log("Selamat datang di Dunia Werewolf, "+nama)
        console.log("Halo "+peran+" "+nama+", kamu akan membantu melindungi temanmu dari serangan werewolf.")
    } else if (peran == "Werewolf"){
        console.log("Selamat datang di Dunia Werewolf, "+nama)
        console.log("Halo "+peran+" "+nama+", kamu akan memakan mangsa setiap malam!")
    }  
}

//Koding untuk tugas switch case
console.log(batas);
console.log("Tugas switch case")

if ((hari>=1)&&(hari<=31)){
} else {
    hari = "**salah tanggal!**"
}

switch(bulan) {
    case 1:   { bulan = "Januari"; break; }
    case 2:   { bulan = "Februari"; break; }
    case 3:   { bulan = "Maret"; break; }
    case 4:   { bulan = "April"; break; }
    case 5:   { bulan = "Mei"; break; }
    case 6:   { bulan = "Juni"; break; }
    case 7:   { bulan = "Juli"; break; }
    case 8:   { bulan = "Agustus"; break; }
    case 9:   { bulan = "September"; break; }
    case 10:   { bulan = "Oktober"; break; }
    case 11:   { bulan = "November"; break; }
    case 12:   { bulan = "Desember"; break; }
    default:  { bulan = "**salah bulan!**"; }
}

if ((tahun>=1900)&&(tahun<=2200)){
} else {
    hari = "**salah tahun!**"
}

console.log(hari+" "+bulan+" "+tahun);